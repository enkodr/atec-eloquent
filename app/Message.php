<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //protected $table = "mensagens";

    function replies() {
        return $this->hasMany('App\Reply');
    }
}
