<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessagesController extends Controller
{
    function list()
    {
        $messages = Message::all();
        $message = Message::find(1);
    
        return view('messages.list', compact('messages'));
    }
}
