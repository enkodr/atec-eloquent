<?php

use Illuminate\Database\Seeder;
use App\Message;
use App\Reply;

class RepliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $message = Message::find(1);
        $reply = new Reply();
        $reply->body = "Reply to message 1";
        //$reply->message->save($message);
        // OR
        $reply->message_id = $message->id;

        $reply->save();

    }
}
