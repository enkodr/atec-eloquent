<?php

use Illuminate\Database\Seeder;
use App\Message;
class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $message = new Message();
        $message->body = 'Message number 1';
        $message->save();
        $message2 = new Message();
        $message2->body = 'Message number 2';
        $message2->save();
    }
}
