<ul>
    @foreach($messages as $message)
    <li>
        {{ $message->body }}
        <ul>
            @foreach($message->replies as $reply)
            <li>{{ $reply->body }}</li>
            @endforeach
        </ul>
    </li>
    @endforeach
</ul>